/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.util;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.addcel.metrix.api.request.PaymentRequest;
import com.addcel.metrix.api.request.SessionQueryRequest;
import com.addcel.metrix.api.response.SessionQueryResponse;
import com.addcel.metrix.entities.ThreatMetrixTransactions;
import com.addcel.metrix.repositories.ThreatMetrixTransactionsRepository;

@Component
public class PersistTmxTransaction {
	
	private static final Logger LOGGER = LogManager.getLogger(PersistTmxTransaction.class);
	
	@Autowired
	private ThreatMetrixTransactionsRepository tmxTransactionsRepo;
	
	public boolean persistRequest(Object request, SessionQueryResponse response) {
		boolean success = false;
		ThreatMetrixTransactions tmxTransaction = new ThreatMetrixTransactions();
		
		tmxTransaction.setRequestResult(response.getRequest_result());
		tmxTransaction.setRequestId(response.getRequest_id());
		if(response.getPolicy_details_api() != null) {
			tmxTransaction.setReviewStatus(response.getPolicy_details_api().getPolicy_detail_api()[0].getCustomer().getReview_status());
			tmxTransaction.setRiskRating(response.getPolicy_details_api().getPolicy_detail_api()[0].getCustomer().getRisk_rating());
			tmxTransaction.setRulesFired(Arrays.toString(response.getPolicy_details_api().getPolicy_detail_api()[0].getCustomer().getRules()));
			tmxTransaction.setTotalScore(response.getPolicy_details_api().getPolicy_detail_api()[0].getCustomer().getScore());
		}
		
		if(request instanceof PaymentRequest) {
			PaymentRequest req = (PaymentRequest) request;
			
			tmxTransaction.setCcBinNumber(req.getCc_bin_number());
			tmxTransaction.setEmail(req.getAccount_email());
			tmxTransaction.setEventType(req.getEvent_type());
			tmxTransaction.setFullname(req.getAccount_name());
			tmxTransaction.setIpAddress(req.getInput_ip_address());
			tmxTransaction.setPolicy(req.getPolicy());
			tmxTransaction.setServiceType(req.getService_type());
			tmxTransaction.setSessionId(req.getSession_id());
			tmxTransaction.setTelephone(req.getAccount_telephone());
			tmxTransaction.setTransactionId(req.getTransaction_id());
			tmxTransaction.setUsername(req.getAccount_login());
		} else {
			SessionQueryRequest req2 = (SessionQueryRequest) request;
			
			tmxTransaction.setEmail(req2.getAccount_email());
			tmxTransaction.setEventType(req2.getEvent_type());
			tmxTransaction.setFullname(req2.getAccount_name());
			tmxTransaction.setIpAddress(req2.getInput_ip_address());
			tmxTransaction.setPolicy(req2.getPolicy());
			tmxTransaction.setServiceType(req2.getService_type());
			tmxTransaction.setSessionId(req2.getSession_id());
			tmxTransaction.setTelephone(req2.getAccount_telephone());
			tmxTransaction.setTransactionId(req2.getTransaction_id());
			tmxTransaction.setUsername(req2.getAccount_login());
		}
		
		try {
			LOGGER.debug("Guardando la investigacion de TMX en la BD ...");
			ThreatMetrixTransactions newTmxTransaction = tmxTransactionsRepo.save(tmxTransaction);

			if(newTmxTransaction != null) {
				LOGGER.debug("Se ha guardado correctamente la investigacion en la BD: " + newTmxTransaction.getId_threat_metrix_transactions());
				success = true;
			} else {
				LOGGER.error("No se pudo persistir en BD la investigacion de TMX");
			}
		} catch(Exception ex) {
			LOGGER.error("Ocurrio un error al persistir en BD la investigacion de TMX: " + ex.getMessage());
			ex.printStackTrace();
		}
		
		return success;
	}

}
