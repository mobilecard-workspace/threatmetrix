/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.util;

import java.io.IOException;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.addcel.metrix.api.response.PolicyDetailsApi;
import com.addcel.metrix.api.response.SessionQueryResponse;
import com.addcel.metrix.constants.ThreatMetrixConstants;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class RestClient {
	
	private ObjectMapper mapper;
	
	private RestClient() {
		mapper = new ObjectMapper();
	}
	
	private static final Logger LOGGER = LogManager.getLogger(RestClient.class);
	
	public SessionQueryResponse createRequest(String uri, Object requestBody) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		String body = mapper.convertValue(requestBody, UriFormat.class).toString();
		
		HttpEntity<String> entity = new HttpEntity<String>(body, addHeaders());
				
		LOGGER.debug("Enviando peticion a la url: " + uri);
		LOGGER.debug("Body Request: " + entity.getBody());
		
		ResponseEntity<String> result = restTemplate.postForEntity(uri, entity, String.class);
		String response = result.getBody();
		
		LOGGER.debug("Status Code: " + result.getStatusCode());
		LOGGER.debug("Body Response: " + response.toString());
		
		return parseJson(response);
	}
	
	private HttpHeaders addHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_FORM_URLENCODED));
		
		return headers;
	}
	
	private SessionQueryResponse parseJson(String json) {
		SessionQueryResponse response = new SessionQueryResponse();
		
		try {
			JsonNode node = mapper.readTree(json);
			response.setRequest_result(mapper.readValue(node.get(ThreatMetrixConstants.REQUEST_RESULT_KEY).traverse(), String.class));
			response.setRequest_id(mapper.readValue(node.get(ThreatMetrixConstants.REQUEST_ID_KEY).traverse(), String.class));
			
			if(response.getRequest_result().equals(ThreatMetrixConstants.REQUEST_RESULT_VAL)) {
				response.setPolicy_details_api(mapper.readValue(node.get(ThreatMetrixConstants.POLICY_DETAILS_API_KEY).traverse(), PolicyDetailsApi.class));
			}
			
			LOGGER.debug("Respuesta mapeada: " + response.toString());
		} catch (IOException ex) {
			LOGGER.error("Error al mapear la respuesta de TMX a JSON");
			ex.printStackTrace();
		}
		
		return response;
	}
	
}
