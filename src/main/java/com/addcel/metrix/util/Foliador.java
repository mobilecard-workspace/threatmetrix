/**
 * @author Victor Ramirez
 */
package com.addcel.metrix.util;

import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.fasterxml.uuid.Generators;

@Component
public class Foliador {

	private static final Logger LOGGER = LogManager.getLogger(Foliador.class);
	
	public String generarFolio() {
		UUID uuid = Generators.timeBasedGenerator().generate();
		String referencia = String.valueOf(uuid.timestamp());
				
		LOGGER.debug("Referencia generada: " + referencia);
		return referencia;
	}
	
}
