/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@PropertySource("classpath:application.properties")
@Data
public class PropertiesFile {
	
	@Value("${metrix.api.url}")
	private String metrixApiUrl;
	
	@Value("${metrix.api.path.session}")
	private String metrixApiPathSession;
	
	@Value("${metrix.api.path.feedback}")
	private String metrixApiPathFeedback;
	
	@Value("${metrix.api.org.id}")
	private String metrixApiOrgId;
	
	@Value("${metrix.api.key}")
	private String metrixApiKey;
	
	@Value("${metrix.api.event.account}")
	private String metrixApiEventAccount;
	
	@Value("${metrix.api.event.payment}")
	private String metrixApiEventPayment;
	
	@Value("${metrix.api.event.login}")
	private String metrixApiEventLogin;
	
	@Value("${metrix.api.event.details}")
	private String metrixApiEventDetails;
	
	@Value("${metrix.api.service.all}")
	private String metrixApiServiceAll;
	
	@Value("${metrix.api.service.device}")
	private String metrixApiServiceDevice;
	
	@Value("${metrix.api.service.did}")
	private String metrixApiServiceDid;
	
	@Value("${metrix.api.service.ip}")
	private String metrixApiServiceIp;
	
	@Value("${metrix.api.service.session}")
	private String metrixApiServiceSession;
	
	@Value("${metrix.api.policy}")
	private String metrixApiPolicy;
	
	@Value("${metrix.api.currency}")
	private String metrixApiCurrency;
	
}
