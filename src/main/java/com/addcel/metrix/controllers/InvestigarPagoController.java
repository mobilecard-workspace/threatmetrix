/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.metrix.request.InvestigarPagoRequest;
import com.addcel.metrix.response.ApiResponse;
import com.addcel.metrix.services.InvestigarPagoService;
import com.fasterxml.uuid.Generators;

@RestController
@RequestMapping("/api")
public class InvestigarPagoController {

	@Autowired
	private InvestigarPagoService invPagoServ;
	
	private static final Logger LOGGER = LogManager.getLogger(InvestigarPagoController.class);
	
	@PostMapping(value = "/investigarPago", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object investigarPago(@RequestBody InvestigarPagoRequest invPagoReq, HttpServletRequest request) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		
		LOGGER.info("Investigado el pago: " + invPagoReq.toString());
		
		ApiResponse resp = invPagoServ.investigaPago(invPagoReq, request.getRemoteAddr());
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}
	
}
