/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
	
    @GetMapping("/heartbeat")
    public String index(){
        return "OK";
    }
	
}
