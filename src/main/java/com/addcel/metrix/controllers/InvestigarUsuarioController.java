/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.metrix.request.InvestigarUsuarioRequest;
import com.addcel.metrix.response.ApiResponse;
import com.addcel.metrix.services.InvestigarUsuarioService;
import com.fasterxml.uuid.Generators;

@RestController
@RequestMapping("/api")
public class InvestigarUsuarioController {
	
	@Autowired
	private InvestigarUsuarioService invUsuarioServ;
	
	private static final Logger LOGGER = LogManager.getLogger(InvestigarUsuarioController.class);
	
	@PostMapping(value = "/investigarUsuario", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object investigarUsuario(@RequestBody InvestigarUsuarioRequest invUsuarioReq, HttpServletRequest request) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		
		LOGGER.info("Investigado al usuario: " + invUsuarioReq.toString());
		
		ApiResponse resp = invUsuarioServ.investigaUsuario(invUsuarioReq, request.getRemoteAddr());
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}

}
