/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.metrix.api.request.PaymentRequest;
import com.addcel.metrix.api.response.Rules;
import com.addcel.metrix.api.response.SessionQueryResponse;
import com.addcel.metrix.constants.StatusConstants;
import com.addcel.metrix.constants.ThreatMetrixConstants;
import com.addcel.metrix.request.InvestigarPagoRequest;
import com.addcel.metrix.response.ApiResponse;
import com.addcel.metrix.response.Reason;
import com.addcel.metrix.util.Foliador;
import com.addcel.metrix.util.PersistTmxTransaction;
import com.addcel.metrix.util.PropertiesFile;
import com.addcel.metrix.util.RestClient;

@Service
public class InvestigarPagoService {

	private static final Logger LOGGER = LogManager.getLogger(InvestigarPagoService.class);
	
	@Autowired
	private RestClient restClient;
	
	@Autowired
	private PropertiesFile propsFile;
	
	@Autowired
	private Foliador foliador;
	
	@Autowired
	private PersistTmxTransaction persistTmxTransaction;
	
	public ApiResponse investigaPago(InvestigarPagoRequest invPagoReq, String ipAddress) {
		LOGGER.debug("IP del usuario: " + ipAddress);
		
		int code = StatusConstants.ERROR_PAGO_CODE;
		String message = StatusConstants.ERROR_PAGO_MSG;
		String tmxError = null;
		String tmxStatus = null;
		String tmxRisk = null;
		Integer tmxScore = null;
		List<Reason> tmxReasons = null;
		
		PaymentRequest paymentReq = new PaymentRequest();
		paymentReq.setAccount_email(invPagoReq.getCorreo());
		paymentReq.setAccount_login(invPagoReq.getUsername());
		paymentReq.setAccount_name(invPagoReq.getNombre());
		paymentReq.setAccount_telephone(invPagoReq.getTelefono());
		paymentReq.setApi_key(propsFile.getMetrixApiKey());
		paymentReq.setEvent_type(propsFile.getMetrixApiEventPayment());
		paymentReq.setInput_ip_address(ipAddress);
		paymentReq.setOrg_id(propsFile.getMetrixApiOrgId());
		paymentReq.setPolicy(propsFile.getMetrixApiPolicy());
		paymentReq.setService_type(propsFile.getMetrixApiServiceAll());
		paymentReq.setSession_id(invPagoReq.getSessionId());
		paymentReq.setTransaction_id(foliador.generarFolio());
		paymentReq.setOutput_format(ThreatMetrixConstants.JSON_OUTPUT_FORMAT);
		paymentReq.setCc_bin_number(invPagoReq.getCardNumber());
		paymentReq.setTransaction_amount(invPagoReq.getCantidad());
		paymentReq.setTransaction_currency(propsFile.getMetrixApiCurrency());
		
		SessionQueryResponse response = null;
		
		try {
			response = restClient.createRequest(propsFile.getMetrixApiUrl() + propsFile.getMetrixApiPathSession(), paymentReq);
		} catch (Exception ex) {
			LOGGER.error("Ocurrio un error al invocar la API de Threat Metrix: " + ex.getMessage());
			ex.printStackTrace();
			return new ApiResponse(StatusConstants.TMX_NOT_AVAILABLE_CODE, StatusConstants.TMX_NOT_AVAILABLE_MSG, tmxStatus, tmxRisk, tmxScore, tmxReasons, tmxError);
		}
		
		if(response.getRequest_result().equals(ThreatMetrixConstants.REQUEST_RESULT_VAL)) {
			LOGGER.info("La API de Threat Metrix pudo investigar correctamente el pago");
			code = StatusConstants.SUCCESS_INVESTIGATION_CODE;
			message = StatusConstants.SUCCESS_INVESTIGATION_MSG;
			
			tmxStatus = response.getPolicy_details_api().getPolicy_detail_api()[0].getCustomer().getReview_status();
			tmxRisk = response.getPolicy_details_api().getPolicy_detail_api()[0].getCustomer().getRisk_rating();
			tmxScore = response.getPolicy_details_api().getPolicy_detail_api()[0].getCustomer().getScore();
			
			LOGGER.info("Estatus de la investigacion: " + tmxStatus);
			LOGGER.info("Riesgo de ese pago: " + tmxRisk);
			LOGGER.debug("Calificacion para ese pago: " + tmxScore);
			
			if(!tmxStatus.equals(ThreatMetrixConstants.STATUS_PASS)) {
				tmxReasons = new ArrayList<Reason>();
				
				for(Rules rule : response.getPolicy_details_api().getPolicy_detail_api()[0].getCustomer().getRules()) {
					tmxReasons.add(new Reason(rule.getReason_code(), rule.getScore()));
				}
				
				LOGGER.debug("Reglas disparadas: " + Arrays.toString(tmxReasons.toArray()));
			}
		} else {
			LOGGER.warn("La API de Threat Metrix no pudo procesar correctamente la peticion: " + tmxError);
			tmxError = response.getRequest_result();
		}
		
		persistTmxTransaction.persistRequest(paymentReq, response);
		
		return new ApiResponse(code, message, tmxStatus, tmxRisk, tmxScore, tmxReasons, tmxError);
	}
	
}
