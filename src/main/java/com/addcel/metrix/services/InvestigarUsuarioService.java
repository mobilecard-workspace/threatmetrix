/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.metrix.api.request.SessionQueryRequest;
import com.addcel.metrix.api.response.Rules;
import com.addcel.metrix.api.response.SessionQueryResponse;
import com.addcel.metrix.constants.StatusConstants;
import com.addcel.metrix.constants.ThreatMetrixConstants;
import com.addcel.metrix.request.InvestigarUsuarioRequest;
import com.addcel.metrix.response.ApiResponse;
import com.addcel.metrix.response.Reason;
import com.addcel.metrix.util.Foliador;
import com.addcel.metrix.util.PersistTmxTransaction;
import com.addcel.metrix.util.PropertiesFile;
import com.addcel.metrix.util.RestClient;

@Service
public class InvestigarUsuarioService {
	
	private static final Logger LOGGER = LogManager.getLogger(InvestigarUsuarioService.class);
	
	@Autowired
	private RestClient restClient;
	
	@Autowired
	private PropertiesFile propsFile;
	
	@Autowired
	private Foliador foliador;
	
	@Autowired
	private PersistTmxTransaction persistTmxTransaction;
	
	public ApiResponse investigaUsuario(InvestigarUsuarioRequest invUsuarioReq, String ipAddress) {
		LOGGER.debug("IP del usuario: " + ipAddress);
		
		int code = StatusConstants.ERROR_INVESTIGATION_CODE;
		String message = StatusConstants.ERROR_INVESTIGATION_MSG;
		String tmxError = null;
		String tmxStatus = null;
		String tmxRisk = null;
		Integer tmxScore = null;
		List<Reason> tmxReasons = null;
		
		SessionQueryRequest sessionQueryReq = new SessionQueryRequest();
		sessionQueryReq.setAccount_email(invUsuarioReq.getCorreo());
		sessionQueryReq.setAccount_login(invUsuarioReq.getUsername());
		sessionQueryReq.setAccount_name(invUsuarioReq.getNombre());
		sessionQueryReq.setAccount_telephone(invUsuarioReq.getTelefono());
		sessionQueryReq.setApi_key(propsFile.getMetrixApiKey());
		sessionQueryReq.setEvent_type(propsFile.getMetrixApiEventAccount());
		sessionQueryReq.setInput_ip_address(ipAddress);
		sessionQueryReq.setOrg_id(propsFile.getMetrixApiOrgId());
		sessionQueryReq.setPolicy(propsFile.getMetrixApiPolicy());
		sessionQueryReq.setService_type(propsFile.getMetrixApiServiceAll());
		sessionQueryReq.setSession_id(invUsuarioReq.getSessionId());
		sessionQueryReq.setTransaction_id(foliador.generarFolio());
		sessionQueryReq.setOutput_format(ThreatMetrixConstants.JSON_OUTPUT_FORMAT);
		
		SessionQueryResponse response = null;
		
		try {
			response = restClient.createRequest(propsFile.getMetrixApiUrl() + propsFile.getMetrixApiPathSession(), sessionQueryReq);
		} catch (Exception ex) {
			LOGGER.error("Ocurrio un error al invocar la API de Threat Metrix: " + ex.getMessage());
			ex.printStackTrace();
			return new ApiResponse(StatusConstants.TMX_NOT_AVAILABLE_CODE, StatusConstants.TMX_NOT_AVAILABLE_MSG, tmxStatus, tmxRisk, tmxScore, tmxReasons, tmxError);
		}
		
		if(response.getRequest_result().equals(ThreatMetrixConstants.REQUEST_RESULT_VAL)) {
			LOGGER.info("La API de Threat Metrix pudo investigar correctamente al usuario");
			code = StatusConstants.SUCCESS_INVESTIGATION_CODE;
			message = StatusConstants.SUCCESS_INVESTIGATION_MSG;
			
			tmxStatus = response.getPolicy_details_api().getPolicy_detail_api()[0].getCustomer().getReview_status();
			tmxRisk = response.getPolicy_details_api().getPolicy_detail_api()[0].getCustomer().getRisk_rating();
			tmxScore = response.getPolicy_details_api().getPolicy_detail_api()[0].getCustomer().getScore();
			
			LOGGER.info("Estatus de la investigacion: " + tmxStatus);
			LOGGER.info("Riesgo de ese usuario: " + tmxRisk);
			LOGGER.debug("Calificacion para este usuario: " + tmxScore);
			
			if(!tmxStatus.equals(ThreatMetrixConstants.STATUS_PASS)) {
				tmxReasons = new ArrayList<Reason>();
				
				for(Rules rule : response.getPolicy_details_api().getPolicy_detail_api()[0].getCustomer().getRules()) {
					tmxReasons.add(new Reason(rule.getReason_code(), rule.getScore()));
				}
				
				LOGGER.debug("Reglas disparadas: " + Arrays.toString(tmxReasons.toArray()));
			}
		} else {
			LOGGER.warn("La API de Threat Metrix no pudo procesar correctamente la peticion: " + tmxError);
			tmxError = response.getRequest_result();
		}
		
		persistTmxTransaction.persistRequest(sessionQueryReq, response);
		
		return new ApiResponse(code, message, tmxStatus, tmxRisk, tmxScore, tmxReasons, tmxError);
	}

}
