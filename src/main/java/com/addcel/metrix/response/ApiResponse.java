/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {

	private Integer code;
	private String message;
	private String tmxStatus;
	private String tmxRisk;
	private Integer tmxScore;
	private List<Reason> tmxReasons;
	private String tmxError;
	
}
