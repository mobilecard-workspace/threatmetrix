/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Reason {

	private String rule;
	private int score;
	
}
