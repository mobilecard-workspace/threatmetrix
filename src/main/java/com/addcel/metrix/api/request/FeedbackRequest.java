/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.api.request;

import lombok.Data;

@Data
public class FeedbackRequest {
	
	private Long idUsuario;
	private Long idEstablecimiento;
	private String conceptoPago;
	
}
