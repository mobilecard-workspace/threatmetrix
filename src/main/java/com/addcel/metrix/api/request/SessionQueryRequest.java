/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.api.request;

import lombok.Data;

@Data
public class SessionQueryRequest {
	
	private String org_id;
	private String api_key;
	private String session_id;
	private String service_type;
	private String event_type;
	private String transaction_id;
	private String input_ip_address;
	private String policy;
	private String account_login;
	private String account_email;
	private String account_telephone;
	private String account_name;
	private String output_format;
	private String application_name;
	
}
