/**
 * @author Victor Ramirez
 */
package com.addcel.metrix.api.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentRequest extends SessionQueryRequest {

	private String cc_bin_number;
	private float transaction_amount;
	private String transaction_currency;
	
}
