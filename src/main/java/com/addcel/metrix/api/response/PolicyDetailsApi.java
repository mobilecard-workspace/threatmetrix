/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.api.response;

import lombok.Data;

@Data
public class PolicyDetailsApi {
	
	private PolicyDetailApi[] policy_detail_api;
	
}
