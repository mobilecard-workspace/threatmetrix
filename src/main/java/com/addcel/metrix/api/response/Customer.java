/**
 * @author Victor
 */

package com.addcel.metrix.api.response;

import lombok.Data;

@Data
public class Customer {
	
	private int score;
	private long pvid;
	private String review_status;
	private String risk_rating;
	private Rules[] rules;

}
