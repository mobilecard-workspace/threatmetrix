/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.api.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SessionQueryResponse {
	
	private String request_result;
	private String request_id;
	private PolicyDetailsApi policy_details_api;
	
}
