/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.api.response;

import lombok.Data;

@Data
public class PolicyDetailApi {

	private String type;
	private int id;
	private Customer customer; 
	
}
