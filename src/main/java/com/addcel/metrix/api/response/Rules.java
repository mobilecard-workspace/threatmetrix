/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.api.response;

import lombok.Data;

@Data
public class Rules {

	private long rid;
	private String reason_code;
	private int score;
	
}
