/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.constants;

public class ThreatMetrixConstants {
	
	public static final String REQUEST_RESULT_KEY = "request_result";
	public static final String REQUEST_ID_KEY = "request_id";
	public static final String POLICY_DETAILS_API_KEY = "policy_details_api";
	
	public static final String REQUEST_RESULT_VAL = "success";
	public static final String JSON_OUTPUT_FORMAT = "json";
	public static final String RISK_HIGH = "high";
	public static final String RISK_MEDIUM = "medium";
	public static final String RISK_LOW = "low";
	public static final String RISK_NEUTRAL = "neutral";
	public static final String RISK_TRUSTED = "trusted";
	public static final String STATUS_PASS = "pass";
	public static final String STATUS_REJECT = "reject";
	public static final String STATUS_REVIEW = "review";
	public static final String STATUS_CHALLENGE = "challenge";
	
}
