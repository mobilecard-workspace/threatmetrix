/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.constants;

public class StatusConstants {

	//Codigos de error
	public static final int SUCCESS_INVESTIGATION_CODE = 0;
	public static final int ERROR_INVESTIGATION_CODE = 2000;
	public static final int ERROR_PAGO_CODE = 3000;
	public static final int TMX_NOT_AVAILABLE_CODE = 5000;
	
	//Mensajes de error
	public static final String SUCCESS_INVESTIGATION_MSG = "Investigacion completada correctamente";
	public static final String ERROR_INVESTIGATION_MSG = "Ocurrio un error al investigar al usuario";
	public static final String ERROR_PAGO_MSG = "Ocurrio un error al investigar el pago";
	public static final String TMX_NOT_AVAILABLE_MSG = "La API de Threat Metrix no esta disponible";
	
}
