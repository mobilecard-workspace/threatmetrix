/**
 * @author Victor Ramirez
 */

package com.addcel.metrix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class ThreatMetrixApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ThreatMetrixApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(ThreatMetrixApplication.class, args);
	}
	
}