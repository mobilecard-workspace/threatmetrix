/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "threat_metrix_transactions")
public class ThreatMetrixTransactions {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id_threat_metrix_transactions;
	
	@Column(name = "username", nullable = false)
	private String username;
	
	@Column(name = "fullname", nullable = false)
	private String fullname;
	
	@Column(name = "email", nullable = false)
	private String email;
	
	@Column(name = "telephone", nullable = false)
	private String telephone;
	
	@Column(name = "cc_bin_number", nullable = true)
	private String ccBinNumber;
	
	@Column(name = "ip_address", nullable = false)
	private String ipAddress;
	
	@Column(name = "transaction_id", nullable = false)
	private String transactionId;
	
	@Column(name = "session_id", nullable = false)
	private String sessionId;
	
	@Column(name = "policy", nullable = false)
	private String policy;
	
	@Column(name = "event_type", nullable = false)
	private String eventType;
	
	@Column(name = "service_type", nullable = false)
	private String serviceType;
	
	@Column(name = "request_result", nullable = false)
	private String requestResult;
	
	@Column(name = "request_id", nullable = false)
	private String requestId;
	
	@Column(name = "total_score", nullable = true)
	private Integer totalScore;
	
	@Column(name = "review_status", nullable = true)
	private String reviewStatus;
	
	@Column(name = "risk_rating", nullable = true)
	private String riskRating;
	
	@Column(name = "rules_fired", nullable = true)
	private String rulesFired;

}
