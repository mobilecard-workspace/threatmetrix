/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InvestigarPagoRequest extends InvestigarUsuarioRequest {

	private String cardNumber;
	private float cantidad;
	
}
