/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.request;

import lombok.Data;

@Data
public class InvestigarUsuarioRequest {
	
	private String sessionId;
	private String nombre;
	private String correo;
	private String username;
	private String telefono;
	private String applicationName;
	
}
