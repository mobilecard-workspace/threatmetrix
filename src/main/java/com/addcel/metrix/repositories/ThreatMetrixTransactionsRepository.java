/**
 * @author Victor Ramirez
 */

package com.addcel.metrix.repositories;

import org.springframework.data.repository.CrudRepository;

import com.addcel.metrix.entities.ThreatMetrixTransactions;

public interface ThreatMetrixTransactionsRepository extends CrudRepository<ThreatMetrixTransactions, Long> {
	
	@SuppressWarnings("unchecked")
	public ThreatMetrixTransactions save(ThreatMetrixTransactions tmxTransactions);

}
